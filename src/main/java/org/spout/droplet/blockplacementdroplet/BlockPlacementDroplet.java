/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.spout.droplet.blockplacementdroplet;

import org.spout.api.command.CommandRegistrationsFactory;
import org.spout.api.command.annotated.AnnotatedCommandRegistrationFactory;
import org.spout.api.command.annotated.SimpleAnnotatedCommandExecutorFactory;
import org.spout.api.command.annotated.SimpleInjector;
import org.spout.api.plugin.CommonPlugin;

/**
 *
 * @author mrodriguez
 */
public class BlockPlacementDroplet extends CommonPlugin {
    
    private static BlockPlacementDroplet plugin;

    @Override
    public void onEnable() {
        plugin = this;
        getLogger().info("Enabled!");
        CommandRegistrationsFactory<Class<?>> commandRegFactory = new AnnotatedCommandRegistrationFactory(new SimpleInjector(this), new SimpleAnnotatedCommandExecutorFactory());
        getEngine().getRootCommand().addSubCommands(this, PlaceBlocks.class, commandRegFactory);
        
    }

    @Override
    public void onDisable() {
        getLogger().info("Disabled!");
        
    }
    
    public static BlockPlacementDroplet getInstance(){
        return plugin;
    }
    
}
