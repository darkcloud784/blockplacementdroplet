/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.spout.droplet.blockplacementdroplet;

import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.entity.Player;
import org.spout.api.geo.World;
import org.spout.api.geo.cuboid.Block;
import org.spout.vanilla.material.VanillaMaterials;

/**
 *
 * @author mrodriguez
 */
public class PlaceBlocks {

    private BlockPlacementDroplet plugin;

    public void PlaceBlocks() {
        plugin = BlockPlacementDroplet.getInstance();
    }

    @Command(aliases = {"place"}, desc = "Places a block where you stand!")
    public void PlaceBlocks(CommandContext args, CommandSource source) {
        Player p;
        //Check if its a player
        if (source instanceof Player) {
            //If its a player, cast source to the player.
            p = (Player) source;
            //Otherwise tell whatever it is that it needs to be a player.
        } else {
            source.sendMessage("You must be a player to use this command!!!");
            return;
        }
        //Get player location, including world.
        World world = p.getScene().getWorld();
        float x = p.getScene().getPosition().getX();
        float y = p.getScene().getPosition().getY();
        float z = p.getScene().getPosition().getZ();
        
        //Place the block where the player is!
        Block block = p.getWorld().getBlock(x, y, z);
        //The block is a dragon egg!!!!
        block.setMaterial(VanillaMaterials.DRAGON_EGG);
    }
}
